# Infinite Data Structures in JavaScript

## Range

Simulates an sorted list of numbers that can have an infinite length.

```javascript
new Range(1, Infinity) // Operates like [1,2,3,...Infinity]
```

### How it works

By taking the starting point, the ending point, and the distance of steps in
between, the range can calculate the value of any point or put out an equivalent
array (if not infinite in size). This also uses for lazy operation when taking a
finite number of entries.

### Use cases

### Properties

#### `from`

The starting point of the range.

#### `to`

The ending point of the range.

#### `by`

The step size of the range.

#### `items`

The total count of entries that would exist in the range if calculated.

#### `isInfinite`

A boolean property for whether or not the range ends.

#### `current`

If you use the range as a lazy operation or an iterator, each time the range
increments, this represents the value of the cursor along the timeline of the
range.

### Methods

#### Constructor

```javascript
new Range(
  from, // starting point, must be a number
  to, // ending point, must be a number
  by // the step
)

// Examples:
const positiveEvenNumbers = new Range(2, Infinity, 2)
const fifths = new Range(0, 1, 0.2)
const allIntegers = new Range(-Infinity, Infinity)
// Ranges can also be "backward"
const countdown = new Range(10, 0, -1)
```

#### `toString`

For getting a string representation of an array without having to evaluate the
full range.

```javascript
// "[1...Infinity]"
const default = new Range().toString()

// "[2,4...Infinity]"
const positiveEvenNumbers = new Range(2, Infinity, 2).toString()

// "[0,0.2...1]"
const fifths = new Range(0, 1, 0.2).toString()

// "[-Infinity...Infinity]"
const allIntegers = new Range(-Infinity, Infinity).toString()

// "[10,9...0]"
const countdown = new Range(10, 0, -1).toString()
```

#### `fromString`

This is the reverse of the previous function, creating a range from a string
representation.

Ignores whitespace, must start with a digit or the capitalized string Infinity,
can be signed negative, must include an ellipsis in the middle, and ends with a
digit or the capitalized string Infinity, which can also be signed negative.

```javascript
Range.fromString("[1...10]") // new Range(1, 10)
Range.fromString("[2,4...10]") // new Range(2, 10, 2)
Range.fromString("[3,2...1]") // new Range(3, 1, -1)
Range.fromString("[-Infinity...Infinity]") // new Range(-Infinity, Infinity)
```

#### `toTuple`

Makes a three-element array with the starting number, ending number, and step size. Can be used as args for a new range.

#### `step`

This increments `current` as well as returning the next value. Optionally takes
a number of times more than 1.

```javascript
const range = new Range()

range.current // 1
range.step() // returns 2
range.current // 2
range.step(10) // returns 12
range.current // 12
```

It also stops going once it's hit the end of the range (if finite).

```javascript
const range = new Range(1, 5)

range.current // 1
range.step() // returns 2
range.current // 2
range.step(10) // returns 5
range.current // 5
range.step() // returns 5, has stopped
range.current // 5
```

#### `isDone`

Recognizes when a range's `current` property has reached the end.

```javascript
const range = new Range(1, 2)

range.current // 1
range.isDone() // returns false
range.step() // returns 2
range.current // 2
range.isDone() // returns true
```

#### `take`

Takes a number of entries "off the top" of the range. Defaults to 1, but can be
more, and also can start from current.

```javascript
const positiveOddNumbers = new Range(1, Infinity, 2)

positiveOddNumbers.take() // [1]
positiveOddNumbers.current // 1
positiveOddNumbers.step() // returns 3
positiveOddNumbers.take(1, true) // [3]
positiveOddNumbers.take(3) // [1,3,5]
```

#### `takeWhile`

Takes a number of entries that satisfy a certain condition (as a function of
type `(n: number) => boolean`), otherwise works the same as `take`.

```javascript
const positiveNumbers = new Range()
const isEven = n => n % 2 === 0

positiveNumbers.takeWhile(isEven) // [2]
positiveNumbers.current // 1
positiveNumbers.step() // returns 2
positiveNumbers.takeWhile(isEven, 1, true) // [2]
positiveNumbers.step() // returns 3
positiveNumbers.takeWhile(isEven, 1, true) // [4]
positiveNumbers.takeWhile(isEven, 3) // [2,4,6]
```

#### `at`

Given a zero-based index like an array, calculates what the range will have at
that point (or returns the memo if already calculated).

```javascript
const positiveNumbers = new Range()

positiveNumbers.at(0) // 1
positiveNumbers.at(10) // 11
positiveNumbers.at(355) // 356
```

#### `contains`

Checks if a value will ever exist within the range.

```javascript
const positiveNumbers = new Range()
const positiveOddNumbers = new Range(1, Infinity, 2)

positiveNumbers.contains(0) // false
positiveNumbers.contains(1) // true

positiveOddNumbers.contains(1) // true
positiveOddNumbers.contains(2) // false
positiveOddNumbers.contains(3) // true
```

#### `isSupersetOf`

Checks if the called range entirely includes another given range.

```javascript
const integers = new Range()
const oddNumbers = new Range(1, Infinity, 2)
const evenNumbers = new Range(2, Infinity, 2)

integers.isSupersetOf(oddNumbers) // true
integers.isSupersetOf(evenNumbers) // true
oddNumbers.isSupersetOf(evenNumbers) // false
evenNumbers.isSupersetOf(oddNumbers) // false

const everyOther = new Range(0, Infinity, 2)
const everyThird = new Range(0, Infinity, 3)
const everyFourth = new Range(0, Infinity, 4)
const everySixth = new Range(0, Infinity, 6)

everyOther.isSupersetOf(everyThird) // false
everyOther.isSupersetOf(everyFourth) // true
everyOther.isSupersetOf(everySixth) // true
everyThird.isSupersetOf(everyFourth) // false
everyThird.isSupersetOf(everySixth) // true
```

### Notes / "gotcha"s

#### Infinity

Because ranges can be infinite and the value of `Infinity` in JavaScript is
anything that is a larger size in memory than a signed number can include, if
any operation evaluates higher than that, every subsequent value will be
`Infinity`. Trying to operate on these values may create unexpected behaviors.
Here is a simple example.

```javascript
const range = new Range()
const justBefore = 2 ** 1024 - 1 // Technically, this already evaluates to Infinity
range.at(justBefore) // Really big
range.at(justBefore + 1) // Infinity
range.at(justBefore + 1) - 1 // still Infinity, not the really big value from before
```

#### Floating point math

There is a [well documented](https://0.30000000000000004.com/) phenomenon when
using floating point precision, binary is off of what should actually be the
case for iterating using floating points. Understand that this will still
evaluate the same way as plain JavaScript, with all the issues that that
entails.

```javascript
const range = new Range(0, 1, 0.1)

range.toArray()
// [
//   0,
//   0.1,
//   0.2,
//   0.30000000000000004,
//   0.4,
//   0.5,
//   0.6000000000000001,
//   0.7000000000000001,
//   0.8,
//   0.9,
//   1
// ]
0.1 * 3 // 0.30000000000000004
```

#### JS `Range`

Technically, `Range` is already the name of a [JavaScript
object](https://developer.mozilla.org/en-US/docs/Web/API/Range). Not a lot to
say about that, just be aware of it.
