"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Utils = __importStar(require("./utils"));
var Stream_1 = __importDefault(require("./Stream"));
var Sequence = (function () {
    function Sequence(initial, generator, options) {
        var _this = this;
        this._initializeMemo = function () {
            _this.initial.forEach(function (value, index) {
                return Utils.memoize(_this._memo, index, value);
            });
            return _this._memo;
        };
        this._precacheTo = function (index) {
            if (index <= _this._memo.size)
                return _this._memo;
            for (var i = _this._memo.size; i <= index; i++)
                _this.at(i);
            return _this._memo;
        };
        this.step = function (times) {
            if (times === void 0) { times = 1; }
            _this._index += times;
            var nextValue = _this.at(_this._index);
            if (nextValue)
                _this.current = nextValue;
            return _this.current;
        };
        this.at = function (index) {
            if (index < 0)
                return undefined;
            if (_this._memo.has(index))
                return _this._memo.get(index);
            var value = _this.generator(index, function (key) { return _this._memo.get(key); });
            Utils.memoize(_this._memo, index, value);
            return value;
        };
        this.isDone = function () {
            return _this.isInfinite ||
                (!!_this._finiteBoundary &&
                    !!_this._finiteBoundary.max &&
                    _this._index >= _this._finiteBoundary.max - 1);
        };
        this.generate = function (from, to) {
            return new Stream_1.default(from, to).toArray().map(function (index) { return _this.at(index); });
        };
        this.take = function (count, fromCurrent) {
            if (count === void 0) { count = 1; }
            if (fromCurrent === void 0) { fromCurrent = false; }
            if (Utils.outOfBounds(count))
                return [];
            var offset = fromCurrent ? _this._index : 0;
            var maxedCount = _this._finiteBoundary && _this._finiteBoundary.max
                ? Math.max(_this._finiteBoundary.max, count)
                : count;
            return Utils.arrayGenerator(maxedCount, function (_, index) {
                return _this.at(index + offset);
            }).filter(function (maybe) { return maybe !== undefined; });
        };
        this.takeWhile = function (condition, count, fromCurrent) {
            if (count === void 0) { count = 1; }
            if (fromCurrent === void 0) { fromCurrent = false; }
            if (Utils.outOfBounds(count))
                return [];
            var copy = new Sequence(_this.initial, _this.generator);
            if (fromCurrent && _this._index > 0)
                copy.step(_this._index);
            return Utils.stepWhile(copy, condition, count);
        };
        this.zip = function (array) {
            return Utils.zipMap(array, _this, array.length);
        };
        var _a = options || {}, _b = _a.cacheToIndex, cacheToIndex = _b === void 0 ? initial.length - 1 : _b, _c = _a.isFinite, isFinite = _c === void 0 ? false : _c, finiteBoundary = _a.finiteBoundary;
        this.initial = initial;
        this._index = 0;
        this.current = this.initial[this._index];
        this.generator = generator;
        this.isInfinite = !isFinite;
        this._memo = new Map();
        this._initializeMemo();
        this._precacheTo(cacheToIndex);
        if (finiteBoundary)
            this._finiteBoundary = finiteBoundary;
    }
    return Sequence;
}());
exports.default = Sequence;
