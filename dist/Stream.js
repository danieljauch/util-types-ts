"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Utils = __importStar(require("./utils"));
var INPUT_ERROR = new Error("Invalid input");
var Stream = (function () {
    function Stream(from, to, by) {
        var _this = this;
        if (from === void 0) { from = 1; }
        if (to === void 0) { to = Infinity; }
        if (by === void 0) { by = 1; }
        this._validateTo = function (to) {
            if (!Number.isFinite(to))
                return to;
            var difference = Number.isFinite(_this.from) ? _this.from : 0;
            if (Math.floor(_this.by) === _this.by) {
                var offset = (to - difference) % _this.by;
                return to - offset;
            }
            return to;
        };
        this.toString = function () {
            if (_this.by === 1 && _this._direction === "up")
                return "[" + _this.from + "..." + _this.to + "]";
            if (isFinite(_this.from))
                return "[" + _this.from + "," + (_this.from + _this.by) + "..." + _this.to + "]";
            if (_this._direction === "up")
                return "[" + _this.from + "," + _this.from + "+" + _this.by + "..." + _this.to + "]";
            return "[" + _this.from + "," + _this.from + _this.by + "..." + _this.to + "]";
        };
        this.toArray = function () { return (_this.isInfinite ? [] : _this._makeArray()); };
        this._makeArray = function (from, to, by) {
            if (from === void 0) { from = _this.from; }
            if (to === void 0) { to = _this.to; }
            if (by === void 0) { by = _this.by; }
            var maxedTo = Math.min(to, _this.to);
            var arr = Utils.arrayGenerator((maxedTo - from) / by + 1, function (_, index) { return index * by + from; });
            var offset = from === _this.from ? 0 : (from - _this.from) / _this.by;
            arr.forEach(function (value, index) {
                return Utils.memoize(_this._memo, index + offset, value);
            });
            return arr;
        };
        this.toTuple = function () { return [
            _this.from,
            _this.to,
            _this.by
        ]; };
        this.step = function (times) {
            if (times === void 0) { times = 1; }
            if (_this.isDone())
                return _this.current;
            if (_this._direction === "up" && _this.current + _this.by * times >= _this.to)
                _this.current = _this.to;
            else if (_this._direction === "down" &&
                _this.current + _this.by * times <= _this.to)
                _this.current = _this.to;
            else
                _this.current += _this.by * times;
            return _this.current;
        };
        this.isDone = function () {
            if (_this.isInfinite)
                return false;
            if (_this.current === _this.to)
                return true;
            if (_this._direction === "up")
                return _this.current > _this.to;
            return _this.current < _this.to;
        };
        this.take = function (count, fromCurrent) {
            if (count === void 0) { count = 1; }
            if (fromCurrent === void 0) { fromCurrent = false; }
            if (Utils.outOfBounds(count))
                return [];
            var offset = _this._direction === "up" ? -1 : 1;
            var start = fromCurrent ? _this.current : _this.from;
            var end = start + count * _this.by + offset;
            return _this._makeArray(start, end);
        };
        this.takeWhile = function (condition, count, fromCurrent) {
            if (count === void 0) { count = 1; }
            if (fromCurrent === void 0) { fromCurrent = false; }
            if (Utils.outOfBounds(count))
                return [];
            var start = fromCurrent ? _this.current : _this.from;
            var copy = new Stream(start, _this.to, _this.by);
            return Utils.stepWhile(copy, condition, count);
        };
        this.at = function (index) {
            if (index < 0 || index >= _this.items)
                return undefined;
            if (_this._memo.has(index))
                return _this._memo.get(index);
            return Utils.memoize(_this._memo, index, _this.from + index * _this.by);
        };
        this.contains = function (n) {
            if (n === _this.from || n === _this.to)
                return true;
            var betweenFromAndTo = _this._direction === "up"
                ? n > _this.from && n < _this.to
                : n < _this.from && n > _this.to;
            var distanceFromStart = n - _this.from;
            var isInStep = distanceFromStart % _this.by === 0;
            if (betweenFromAndTo && isInStep) {
                Utils.memoize(_this._memo, distanceFromStart / _this.by, n);
                return true;
            }
            return false;
        };
        this.isSupersetOf = function (other) {
            return Math.sign(_this.by) === Math.sign(other.by) &&
                other.by % _this.by === 0 &&
                _this.contains(other.from) &&
                _this.contains(other.to);
        };
        this.zip = function (array) {
            return Utils.zipMap(array, _this, Math.min(array.length, _this.items));
        };
        if ((by > 0 && to < from) || (by < 0 && to > from) || by === 0)
            throw INPUT_ERROR;
        this._memo = new Map();
        this._direction = by > 0 ? "up" : "down";
        this.from = from;
        Utils.memoize(this._memo, 0, this.from);
        this.by = by;
        this.to = this._validateTo(to);
        this.isInfinite = this.from === -Infinity || this.to === Infinity;
        this.items = this.isInfinite
            ? Infinity
            : (this.to - this.from) / this.by + 1;
        Utils.memoize(this._memo, this.items - 1, this.to);
        this.current = this.from;
    }
    Stream.fromString = function (rawInput) {
        var stripped = rawInput.replace(/\s/g, "");
        var validated = /^\[(-?(\d+|(Infinity)),)?(-?(\d+|(Infinity)),?)[.]{3}(-?(\d+|(Infinity)))\]$/.exec(stripped);
        if (!validated)
            throw INPUT_ERROR;
        var stripBrackets = validated[0].replace(/\[|\]/g, "");
        var entries = stripBrackets.split(/,|\.{3}/g);
        var numbers = entries.map(function (entry) { return Number(entry); });
        if (numbers.find(function (n) { return Number.isNaN(n); }))
            throw INPUT_ERROR;
        if (numbers.length === 2) {
            var from = numbers[0], to = numbers[1];
            return new Stream(from, to);
        }
        if (numbers.length === 3) {
            var from = numbers[0], next = numbers[1], to = numbers[2];
            var by = next - from;
            return new Stream(from, to, by);
        }
        throw INPUT_ERROR;
    };
    return Stream;
}());
exports.default = Stream;
