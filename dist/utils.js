"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.zipMap = exports.stepWhile = exports.outOfBounds = exports.memoize = exports.arrayGenerator = void 0;
exports.arrayGenerator = function (count, generator) { return Array.from(new Array(count), generator); };
exports.memoize = function (memo, index, value) {
    if (memo.has(index))
        return value;
    memo.set(index, value);
    return value;
};
exports.outOfBounds = function (value, min, max) {
    if (min === void 0) { min = 1; }
    if (max === void 0) { max = Infinity; }
    return value < min || value >= max;
};
exports.stepWhile = function (ids, condition, count) {
    if (count === void 0) { count = 1; }
    var remainingCount = count;
    var output = [];
    while (remainingCount > 0) {
        if (condition(ids.current)) {
            output.push(ids.current);
            remainingCount--;
        }
        if (!ids.isInfinite && ids.isDone())
            break;
        ids.step();
    }
    return output;
};
var isArray = function (zipable) {
    return zipable instanceof Array;
};
var take = function (zipable, index) {
    return isArray(zipable) ? zipable[index] : zipable.at(index);
};
exports.zipMap = function (left, right, count) {
    return Array.from(new Array(count), function (_, index) {
        var leftItem = take(left, index);
        var rightItem = take(right, index);
        if (leftItem && rightItem)
            return [leftItem, rightItem];
        return undefined;
    }).filter(function (item) { return item !== undefined; });
};
