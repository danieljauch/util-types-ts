"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Utils = __importStar(require("./utils"));
var Cycle = (function () {
    function Cycle() {
        var _this = this;
        var values = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            values[_i] = arguments[_i];
        }
        this.step = function (times) {
            if (times === void 0) { times = 1; }
            if (_this._index + 1 === _this.values.length)
                _this._index = 0;
            else
                _this._index += 1;
            _this.current = _this.values[_this._index];
            if (times > 1)
                return _this.step(times - 1);
            return _this.current;
        };
        this.at = function (index) {
            return index >= 0 && Number.isFinite(index)
                ? _this.values[index % _this.values.length]
                : undefined;
        };
        this.isDone = function () { return false; };
        this.take = function (count, fromCurrent) {
            if (count === void 0) { count = 1; }
            if (fromCurrent === void 0) { fromCurrent = false; }
            if (Utils.outOfBounds(count))
                return [];
            var copy = new (Cycle.bind.apply(Cycle, __spreadArrays([void 0], _this.values)))();
            if (fromCurrent && _this._index > 0)
                copy.step(_this._index);
            return Utils.arrayGenerator(count, function () {
                var value = copy.current;
                copy.step();
                return value;
            });
        };
        this.takeWhile = function (condition, count, fromCurrent) {
            if (count === void 0) { count = 1; }
            if (fromCurrent === void 0) { fromCurrent = false; }
            if (Utils.outOfBounds(count))
                return [];
            var copy = new (Cycle.bind.apply(Cycle, __spreadArrays([void 0], _this.values)))();
            if (fromCurrent && _this._index > 0)
                copy.step(_this._index);
            return Utils.stepWhile(copy, condition, count);
        };
        this.map = function (mapper) {
            return new (Cycle.bind.apply(Cycle, __spreadArrays([void 0], _this.values.map(mapper))))();
        };
        this._lowestCombinedLength = function (l, r) {
            if (l === r)
                return l;
            if (l > r && l % r === 0)
                return l;
            if (r > l && r % l === 0)
                return r;
            return l * r;
        };
        this.zip = function (array) {
            return Utils.zipMap(array, _this, array.length);
        };
        this.zipWith = function (other) {
            var length = _this._lowestCombinedLength(_this.values.length, other.values.length);
            var left = _this.take(length);
            var right = other.take(length);
            return new (Cycle.bind.apply(Cycle, __spreadArrays([void 0], Utils.arrayGenerator(length, function (_, index) { return [left[index], right[index]]; }))))();
        };
        this.values = values;
        this.isInfinite = true;
        this._index = 0;
        this.current = this.values[this._index];
    }
    return Cycle;
}());
exports.default = Cycle;
