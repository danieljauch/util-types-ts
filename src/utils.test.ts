import {expect} from "chai"

import * as Utils from "./utils"
import Cycle from "./Cycle"
import Sequence from "./Sequence"
import Stream from "./Stream"

describe("Utils", () => {
  describe("arrayGenerator()", () => {
    it("The given count matches the length of the output", () => {
      const size = 3
      const result = Utils.arrayGenerator(size, (_, index) => index)

      expect(result.length).to.equal(size)
    })

    it("matches generator function", () => {
      const result = Utils.arrayGenerator(3, (_, index) => index)

      expect(result).to.eql([0, 1, 2])
    })
  })

  describe("memoize()", () => {
    it("returns the already memoized value if exists", () => {
      const obj = {a: 1}
      const index = 0
      const memo = new Map() as Map<number, Object>
      memo.set(index, obj)

      expect(Utils.memoize(memo, index, obj)).to.equal(obj) // Same ref
    })

    it("otherwise adds it", () => {
      const value = 1
      const index = 0
      const memo = new Map() as Map<number, number>

      expect(memo.has(index)).to.be.false
      expect(Utils.memoize(memo, index, value)).to.equal(value)
      expect(memo.has(index)).to.be.true
    })
  })

  describe("outOfBounds()", () => {
    it("recognizes boundaries when given", () => {
      const min = 0
      const max = 1

      expect(Utils.outOfBounds(-1, min, max)).to.be.true
      expect(Utils.outOfBounds(0, min, max)).to.be.false
      expect(Utils.outOfBounds(1, min, max)).to.be.true
    })

    it("defaults to min 1 and max Infinity", () => {
      expect(Utils.outOfBounds(0)).to.be.true
      expect(Utils.outOfBounds(1)).to.be.false
      expect(Utils.outOfBounds(Infinity)).to.be.true
    })
  })

  describe("stepWhile()", () => {
    it("increments properly, reguardless of infinite structure", () => {
      const cycle = new Cycle(1, 2)
      const sequence = new Sequence([1], (index, _accessor) => index + 1)
      const stream = new Stream()
      const always = () => true

      expect(cycle.current).to.equal(1)
      expect(sequence.current).to.equal(1)
      expect(stream.current).to.equal(1)

      Utils.stepWhile(cycle, always)
      Utils.stepWhile(sequence, always)
      Utils.stepWhile(stream, always)

      expect(cycle.current).to.equal(2)
      expect(sequence.current).to.equal(2)
      expect(stream.current).to.equal(2)
    })
  })

  describe("zipMap()", () => {
    it("works with IDS's", () => {
      const cycle = new Cycle(1, 2)
      const stream = new Stream()

      expect(Utils.zipMap(cycle, stream, 10)).to.eql([
        [1, 1],
        [2, 2],
        [1, 3],
        [2, 4],
        [1, 5],
        [2, 6],
        [1, 7],
        [2, 8],
        [1, 9],
        [2, 10]
      ])
    })

    it("works with arrays", () => {
      const array = [5, 4, 3, 2, 1]
      const stream = new Stream()

      expect(Utils.zipMap(array, stream, array.length)).to.eql([
        [5, 1],
        [4, 2],
        [3, 3],
        [2, 4],
        [1, 5]
      ])
    })
  })
})
