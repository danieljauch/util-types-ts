import * as Utils from "./utils"
import {IDS, Maybe, Tuple} from "./types"

export default class Cycle<T> implements IDS<T> {
  private _index: number

  readonly values: T[]
  readonly isInfinite: boolean

  current: T

  constructor(...values: T[]) {
    this.values = values
    this.isInfinite = true
    this._index = 0
    this.current = this.values[this._index]
  }

  step = (times = 1): T => {
    if (this._index + 1 === this.values.length) this._index = 0
    else this._index += 1

    this.current = this.values[this._index]
    if (times > 1) return this.step(times - 1)
    return this.current
  }

  at = (index: number): Maybe<T> =>
    index >= 0 && Number.isFinite(index)
      ? this.values[index % this.values.length]
      : undefined

  isDone = (): boolean => false

  take = (count = 1, fromCurrent = false): T[] => {
    if (Utils.outOfBounds(count)) return []
    const copy = new Cycle(...this.values)
    if (fromCurrent && this._index > 0) copy.step(this._index)

    return Utils.arrayGenerator<T>(count, () => {
      const value = copy.current
      copy.step()
      return value
    }) as T[]
  }

  takeWhile = (
    condition: (item: T) => boolean,
    count = 1,
    fromCurrent = false
  ): T[] => {
    if (Utils.outOfBounds(count)) return []
    const copy = new Cycle(...this.values)
    if (fromCurrent && this._index > 0) copy.step(this._index)
    return Utils.stepWhile(copy, condition, count)
  }

  map = <U>(mapper: (item: T) => U): Cycle<U> =>
    new Cycle(...this.values.map(mapper))

  private _lowestCombinedLength = (l: number, r: number): number => {
    if (l === r) return l
    if (l > r && l % r === 0) return l
    if (r > l && r % l === 0) return r
    return l * r
  }

  zip = <U>(array: U[]): Tuple<U, T>[] =>
    Utils.zipMap<U, T>(array, this, array.length)

  zipWith = <U>(other: Cycle<U>): Cycle<Tuple<T, U>> => {
    const length = this._lowestCombinedLength(
      this.values.length,
      other.values.length
    )
    const left = this.take(length)
    const right = other.take(length)

    return new Cycle(
      ...(Utils.arrayGenerator<Tuple<T, U>>(
        length,
        (_, index) => [left[index], right[index]] as Tuple<T, U>
      ) as Tuple<T, U>[])
    )
  }
}
