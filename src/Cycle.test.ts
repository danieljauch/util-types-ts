import {expect} from "chai"

import Cycle from "./Cycle"

describe("Cycle", () => {
  describe("constructor()", () => {
    it("properly defaults", () => {
      const cycle = new Cycle(1, 2, 3)

      expect(cycle.values).to.eql([1, 2, 3])
      expect(cycle.current).to.equal(1)
    })
  })

  describe("step()", () => {
    it("correctly increments one at a time", () => {
      const cycle = new Cycle(1, 2, 3)

      expect(cycle.current).to.equal(1)
      expect(cycle.step()).to.equal(2)
      expect(cycle.current).to.equal(2)
    })

    it("works with repeated values", () => {
      const cycle = new Cycle(1, 1, 2)

      expect(cycle.current).to.equal(1)
      cycle.step()
      expect(cycle.current).to.equal(1)
      cycle.step()
      expect(cycle.current).to.equal(2)
    })

    it("wraps around properly", () => {
      const cycle = new Cycle(1, 2)

      expect(cycle.current).to.equal(1)
      cycle.step()
      expect(cycle.current).to.equal(2)
      cycle.step()
      expect(cycle.current).to.equal(1)
    })
  })

  describe("at()", () => {
    it("doesn't look for something out of range", () => {
      const cycle = new Cycle(1, 2, 3)

      expect(cycle.at(-1)).to.be.undefined
      expect(cycle.at(Infinity)).to.be.undefined
    })

    it("gets the value", () => {
      const cycle = new Cycle(1, 2, 3)

      expect(cycle.at(0)).to.equal(1)
      expect(cycle.at(1)).to.equal(2)
      expect(cycle.at(2)).to.equal(3)

      expect(cycle.at(3)).to.equal(1)
      expect(cycle.at(100)).to.equal(2)
    })
  })

  describe("take()", () => {
    it("returns an empty list when given an invalid count", () => {
      const cycle = new Cycle(1, 2, 3)

      expect(cycle.take(0)).to.eql([])
      expect(cycle.take(Infinity)).to.eql([])
    })

    it("properly continues from the current point", () => {
      const cycle = new Cycle(1, 2, 3)

      expect(cycle.current).to.equal(1)
      cycle.step()
      expect(cycle.current).to.equal(2)
      expect(cycle.take(2, true)).to.eql([2, 3])
    })

    it("defaults to one item", () => {
      const cycle = new Cycle(1, 2, 3)

      expect(cycle.take().length).to.equal(1)
    })

    it("starts from the beginning by default", () => {
      const cycle = new Cycle(1, 2, 3)

      cycle.step()
      expect(cycle.take()).to.eql([1])
      expect(cycle.take(3)).to.eql([1, 2, 3])
    })

    it("can keep taking past the end of the cycle", () => {
      const cycle = new Cycle(1, 2, 3)

      expect(cycle.take(7)).to.eql([1, 2, 3, 1, 2, 3, 1])
    })
  })

  describe("takeWhile()", () => {
    const isOdd = (n: number): boolean => n % 2 > 0

    it("returns an empty list when given an invalid count", () => {
      const cycle = new Cycle(1, 2, 3)

      expect(cycle.takeWhile(isOdd, 0)).to.eql([])
      expect(cycle.takeWhile(isOdd, Infinity)).to.eql([])
    })

    it("properly continues from the current point", () => {
      const cycle = new Cycle(1, 2, 3)

      expect(cycle.current).to.equal(1)
      cycle.step()
      expect(cycle.current).to.equal(2)
      expect(cycle.takeWhile(isOdd, 2, true)).to.eql([3, 1])
      cycle.step(2)
      expect(cycle.current).to.equal(1)
      expect(cycle.takeWhile(isOdd, 2, true)).to.eql([1, 3])
    })

    it("defaults to one item", () => {
      const cycle = new Cycle(1, 2, 3)

      expect(cycle.takeWhile(isOdd).length).to.equal(1)
    })

    it("starts from the beginning by default", () => {
      const cycle = new Cycle(1, 2, 3)

      cycle.step(2)
      expect(cycle.takeWhile(isOdd, 1)).to.eql([1])
      expect(cycle.takeWhile(isOdd, 3)).to.eql([1, 3, 1])
    })
  })

  describe("map()", () => {
    const double = (n: number): number => n * 2

    it("makes a new cycle with altered values", () => {
      const cycle = new Cycle(1, 2, 3)

      expect(cycle.map(double).values).to.eql([2, 4, 6])
    })
  })

  describe("zip()", () => {
    it("zips up length of given array", () => {
      const array = ["a", "b", "c"]
      const cycle = new Cycle(1, 2, 3)

      expect(cycle.zip(array).length).to.equal(array.length)
    })

    it("matches types properly", () => {
      const array = ["a", "b", "c"]
      const cycle = new Cycle(1, 2, 3)

      expect(cycle.zip(array)).to.eql([
        ["a", 1],
        ["b", 2],
        ["c", 3]
      ])
    })

    it("cycles during zip", () => {
      const array = ["a", "b", "c", "d", "e"]
      const cycle = new Cycle(1, 2)

      expect(cycle.zip(array)).to.eql([
        ["a", 1],
        ["b", 2],
        ["c", 1],
        ["d", 2],
        ["e", 1]
      ])
    })
  })

  describe("zipWith()", () => {
    it("combines two cycles of equal length at the same length", () => {
      const c1 = new Cycle(1, 2, 3)
      const c2 = new Cycle(4, 5, 6)

      expect(c1.zipWith(c2).values).to.eql([
        [1, 4],
        [2, 5],
        [3, 6]
      ])
    })

    it("combines with minimum entries if one is equal to a multiple of the size of the other", () => {
      const c1 = new Cycle(1, 2, 3, 4)
      const c2 = new Cycle(5, 6)

      expect(c1.zipWith(c2).values).to.eql([
        [1, 5],
        [2, 6],
        [3, 5],
        [4, 6]
      ])
    })

    it("can zip different types", () => {
      const c1 = new Cycle("a", "b")
      const c2 = new Cycle(1, 2)

      expect(c1.zipWith(c2).values).to.eql([
        ["a", 1],
        ["b", 2]
      ])
    })

    it("multiplies the lengths if they're incompatible", () => {
      const c1 = new Cycle(1, 2, 3)
      const c2 = new Cycle(4, 5)

      expect(c1.zipWith(c2).values).to.eql([
        [1, 4],
        [2, 5],
        [3, 4],
        [1, 5],
        [2, 4],
        [3, 5]
      ])
    })
  })
})
