import {expect} from "chai"

import Sequence from "./Sequence"
import {Maybe, Tuple} from "./types"

describe("Sequence", () => {
  describe("constructor()", () => {
    it("properly defaults", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.initial).to.eql([1])
      expect(sequence.current).to.equal(1)
      expect(sequence.generator).to.equal(generator)
    })
  })

  describe("step()", () => {
    it("increments current by step", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.current).to.equal(1)
      sequence.step()
      expect(sequence.current).to.equal(2)
    })

    it("can optionally take a number of steps", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.current).to.equal(1)
      sequence.step(10)
      expect(sequence.current).to.equal(11)
    })
  })

  describe("at()", () => {
    it("returns undefined for a value out of range", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.at(-1)).to.be.undefined
    })

    it("returns a value properly in sequence", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.at(0)).to.equal(initial[0])
      expect(sequence.at(1)).to.equal(generator(1))
    })
  })

  describe("generate()", () => {
    it("can collect the calculated values between the given arguments", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.generate(9, 14)).to.eql([10, 11, 12, 13, 14, 15])
    })

    it("returns undefined entries for out of range values", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.generate(-1, 1)).to.eql([undefined, 1, 2])
    })
  })

  describe("take()", () => {
    it("returns an empty list when given an invalid count", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.take(0)).to.eql([])
      expect(sequence.take(Infinity)).to.eql([])
    })

    it("properly continues from the current point", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.current).to.equal(1)
      sequence.step()
      expect(sequence.current).to.equal(2)
      expect(sequence.take(2, true)).to.eql([2, 3])
    })

    it("defaults to one item", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.take().length).to.equal(1)
    })

    it("starts from the beginning by default", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      sequence.step()
      expect(sequence.take()).to.eql([1])
      expect(sequence.take(3)).to.eql([1, 2, 3])
    })
  })

  describe("takeWhile()", () => {
    const isOdd = (n: number): boolean => n % 2 > 0

    it("returns an empty list when given an invalid count", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.takeWhile(isOdd, 0)).to.eql([])
      expect(sequence.takeWhile(isOdd, Infinity)).to.eql([])
    })

    it("properly continues from the current point", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.current).to.equal(1)
      sequence.step()
      expect(sequence.current).to.equal(2)
      expect(sequence.takeWhile(isOdd, 2, true)).to.eql([3, 5])
    })

    it("defaults to one item", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.takeWhile(isOdd).length).to.equal(1)
    })

    it("starts from the beginning by default", () => {
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      sequence.step(2)
      expect(sequence.takeWhile(isOdd, 1)).to.eql([1])
      expect(sequence.takeWhile(isOdd, 3)).to.eql([1, 3, 5])
    })
  })

  describe("zip()", () => {
    it("zips up length of given array", () => {
      const array = ["a", "b", "c"]
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.zip(array).length).to.equal(array.length)
    })

    it("matches types properly", () => {
      const array = ["a", "b", "c"]
      const initial = [1]
      const generator = (index: number) => index + 1
      const sequence = new Sequence(initial, generator)

      expect(sequence.zip(array)).to.eql([
        ["a", 1],
        ["b", 2],
        ["c", 3]
      ])
    })
  })

  describe("use cases", () => {
    it("works with fibonacci", () => {
      const initial = [0, 1]
      const generator = (
        index: number,
        getMemoAt: (index: number) => Maybe<number>
      ): number => {
        if (index < 2) return initial[index]
        const last1 = getMemoAt(index - 1) || generator(index - 1, getMemoAt)
        const last2 = getMemoAt(index - 2) || generator(index - 2, getMemoAt)
        return last1 + last2
      }
      const sequence = new Sequence(initial, generator, {cacheToIndex: 50})

      expect(sequence.at(50)).to.equal(12586269025)
      // Last benchmark: 4ms
    })

    it("works with collatz", () => {
      type CollatzTuple = [number: number, steps: number]
      const initial = [
        [0, 0],
        [1, 0]
      ] as CollatzTuple[]
      const generator = (
        index: number,
        getMemoAt: (index: number) => Maybe<CollatzTuple>
      ): CollatzTuple => {
        const isOdd = (n: number): boolean => n % 2 > 0
        const next = (n: number): number => (isOdd(n) ? n * 3 + 1 : n / 2)
        const collatz = (n: number, steps = 0): number => {
          if (n <= 1) return steps
          const memoizedValue = getMemoAt(n)
          if (memoizedValue) return memoizedValue[1] + steps
          return collatz(next(n), steps + 1)
        }

        return getMemoAt(index) || [index, collatz(index)]
      }
      const sequence = new Sequence(initial, generator)

      expect(sequence.at(100)).to.eql([100, 25])
      // Last benchmark: 6ms
    })
  })
})
