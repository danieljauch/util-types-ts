export type Maybe<T> = T | undefined
export type valof<T> = T[keyof T]

export type Tuple<T, U> = [left: T, right: U]
export type Memo<T> = Map<number, T>
export interface Range {
  min: number
  max: number
}

export type Zipable<T> = Array<T> | IDS<T>
// Infinite Data Structure interface
export interface IDS<T> {
  current: T
  isInfinite: boolean

  step: (times?: number) => T
  at: (index: number) => Maybe<T>
  isDone: () => boolean

  take: (count?: number, fromCurrent?: boolean) => T[]
  takeWhile: (
    condition: (item: T) => boolean,
    count?: number,
    fromCurrent?: boolean
  ) => T[]

  zip: <U>(Zipable: U[]) => Tuple<U, T>[]
}
