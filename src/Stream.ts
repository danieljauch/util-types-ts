import * as Utils from "./utils"
import {IDS, Maybe, Tuple} from "./types"

const INPUT_ERROR = new Error("Invalid input")

export default class Stream implements IDS<number> {
  private _memo: Map<number, number>
  private _direction: "up" | "down"

  readonly from: number
  readonly to: number
  readonly by: number
  readonly items: number
  readonly isInfinite: boolean

  current: number

  constructor(from = 1, to = Infinity, by = 1) {
    if ((by > 0 && to < from) || (by < 0 && to > from) || by === 0)
      throw INPUT_ERROR

    this._memo = new Map() as Map<number, number>
    this._direction = by > 0 ? "up" : "down"
    this.from = from
    Utils.memoize(this._memo, 0, this.from)
    this.by = by
    this.to = this._validateTo(to)
    this.isInfinite = this.from === -Infinity || this.to === Infinity
    this.items = this.isInfinite
      ? Infinity
      : (this.to - this.from) / this.by + 1
    Utils.memoize(this._memo, this.items - 1, this.to)
    this.current = this.from
  }

  private _validateTo = (to: number): number => {
    if (!Number.isFinite(to)) return to
    const difference = Number.isFinite(this.from) ? this.from : 0
    if (Math.floor(this.by) === this.by) {
      const offset = (to - difference) % this.by
      return to - offset
    }
    return to
  }

  toString = (): string => {
    if (this.by === 1 && this._direction === "up")
      return `[${this.from}...${this.to}]`
    if (isFinite(this.from))
      return `[${this.from},${this.from + this.by}...${this.to}]`
    if (this._direction === "up")
      return `[${this.from},${this.from}+${this.by}...${this.to}]`
    return `[${this.from},${this.from}${this.by}...${this.to}]`
  }

  static fromString = (rawInput: string): Stream => {
    const stripped = rawInput.replace(/\s/g, "")
    const validated = /^\[(-?(\d+|(Infinity)),)?(-?(\d+|(Infinity)),?)[.]{3}(-?(\d+|(Infinity)))\]$/.exec(
      stripped
    )
    if (!validated) throw INPUT_ERROR
    const stripBrackets = validated[0].replace(/\[|\]/g, "")
    const entries = stripBrackets.split(/,|\.{3}/g)
    const numbers = entries.map(entry => Number(entry))
    if (numbers.find(n => Number.isNaN(n))) throw INPUT_ERROR
    if (numbers.length === 2) {
      const [from, to] = numbers
      return new Stream(from, to)
    }
    if (numbers.length === 3) {
      const [from, next, to] = numbers
      const by = next - from
      return new Stream(from, to, by)
    }
    throw INPUT_ERROR
  }

  toArray = (): number[] => (this.isInfinite ? [] : this._makeArray())

  private _makeArray = (
    from = this.from,
    to = this.to,
    by = this.by
  ): number[] => {
    const maxedTo = Math.min(to, this.to)

    const arr = Utils.arrayGenerator<number>(
      (maxedTo - from) / by + 1,
      (_, index) => index * by + from
    )
    const offset = from === this.from ? 0 : (from - this.from) / this.by
    arr.forEach((value, index) =>
      Utils.memoize(this._memo, index + offset, value)
    )

    return arr as number[]
  }

  toTuple = (): [from: number, to: number, by: number] => [
    this.from,
    this.to,
    this.by
  ]

  step = (times = 1): number => {
    if (this.isDone()) return this.current
    if (this._direction === "up" && this.current + this.by * times >= this.to)
      this.current = this.to
    else if (
      this._direction === "down" &&
      this.current + this.by * times <= this.to
    )
      this.current = this.to
    else this.current += this.by * times
    return this.current
  }

  isDone = (): boolean => {
    if (this.isInfinite) return false
    if (this.current === this.to) return true
    if (this._direction === "up") return this.current > this.to
    return this.current < this.to
  }

  take = (count = 1, fromCurrent = false): number[] => {
    if (Utils.outOfBounds(count)) return []
    const offset = this._direction === "up" ? -1 : 1
    const start = fromCurrent ? this.current : this.from
    const end = start + count * this.by + offset
    return this._makeArray(start, end)
  }

  takeWhile = (
    condition: (n: number) => boolean,
    count = 1,
    fromCurrent = false
  ): number[] => {
    if (Utils.outOfBounds(count)) return []
    const start = fromCurrent ? this.current : this.from
    const copy = new Stream(start, this.to, this.by)
    return Utils.stepWhile(copy, condition, count)
  }

  at = (index: number): Maybe<number> => {
    if (index < 0 || index >= this.items) return undefined
    if (this._memo.has(index)) return this._memo.get(index)
    return Utils.memoize(this._memo, index, this.from + index * this.by)
  }

  contains = (n: number): boolean => {
    if (n === this.from || n === this.to) return true

    const betweenFromAndTo =
      this._direction === "up"
        ? n > this.from && n < this.to
        : n < this.from && n > this.to
    const distanceFromStart = n - this.from
    const isInStep = distanceFromStart % this.by === 0

    if (betweenFromAndTo && isInStep) {
      Utils.memoize(this._memo, distanceFromStart / this.by, n)
      return true
    }
    return false
  }

  isSupersetOf = (other: Stream): boolean =>
    Math.sign(this.by) === Math.sign(other.by) &&
    other.by % this.by === 0 &&
    this.contains(other.from) &&
    this.contains(other.to)

  zip = <T>(array: T[]): Tuple<T, number>[] =>
    Utils.zipMap<T, number>(array, this, Math.min(array.length, this.items))
}
