import {IDS, Maybe, Memo, Tuple, Zipable} from "./types"

export const arrayGenerator = <T>(
  count: number,
  generator: (v: T, k: number) => Maybe<T>
): Maybe<T>[] => Array.from(new Array(count), generator)

export const memoize = <T>(memo: Memo<T>, index: number, value: T): T => {
  if (memo.has(index)) return value
  memo.set(index, value)
  return value
}

export const outOfBounds = (value: number, min = 1, max = Infinity): boolean =>
  value < min || value >= max

export const stepWhile = <T>(
  ids: IDS<T>,
  condition: (item: T) => boolean,
  count = 1
): T[] => {
  let remainingCount = count
  const output = []

  while (remainingCount > 0) {
    if (condition(ids.current)) {
      output.push(ids.current)
      remainingCount--
    }
    if (!ids.isInfinite && ids.isDone()) break
    ids.step()
  }

  return output
}

const isArray = <T>(zipable: Zipable<T>): zipable is Array<T> =>
  zipable instanceof Array

const take = <T>(zipable: Zipable<T>, index: number): Maybe<T> =>
  isArray(zipable) ? zipable[index] : zipable.at(index)

export const zipMap = <T, U>(
  left: Zipable<T>,
  right: Zipable<U>,
  count: number
): Tuple<T, U>[] =>
  Array.from(new Array(count), (_, index) => {
    const leftItem = take(left, index)
    const rightItem = take(right, index)
    if (leftItem && rightItem) return [leftItem, rightItem] as Tuple<T, U>
    return undefined
  }).filter(item => item !== undefined) as Tuple<T, U>[]
