import {expect} from "chai"

import Stream from "./Stream"

describe("Stream", () => {
  describe("constructor()", () => {
    it("properly defaults", () => {
      const stream = new Stream()

      expect(stream.from).to.equal(1)
      expect(stream.to).to.equal(Infinity)
      expect(stream.by).to.equal(1)
      expect(stream.isInfinite).to.be.true
      expect(stream.items).to.equal(Infinity)
      expect(stream.current).to.equal(1)
    })

    it("corrects 'to' value when outside the stream", () => {
      const stream = new Stream(1, 10, 2)

      expect(stream.to).to.equal(9)
    })

    it("recognizes an invalid stream", () => {
      expect(() => new Stream(1, 0)).to.throw()
      expect(() => new Stream(0, 1, -1)).to.throw()
      expect(() => new Stream(1, 1, 0)).to.throw()
    })

    it("properly maps when all arguments are given", () => {
      const stream = new Stream(-Infinity, 0, 0.01)

      expect(stream.from).to.equal(-Infinity)
      expect(stream.to).to.equal(0)
      expect(stream.by).to.equal(0.01)
      expect(stream.isInfinite).to.be.true
      expect(stream.items).to.equal(Infinity)
      expect(stream.current).to.equal(-Infinity)
    })

    it("recognizes a finite stream", () => {
      const stream = new Stream(1, 5)

      expect(stream.isInfinite).to.be.false
    })
  })

  describe("toString()", () => {
    it("states it simply when 'by' is implied", () => {
      expect(new Stream().toString()).to.equal("[1...Infinity]")
      expect(new Stream(-5, 5).toString()).to.equal("[-5...5]")
    })

    it("gives steps when 'by' is custom", () => {
      expect(new Stream(1, 10, 0.25).toString()).to.equal("[1,1.25...10]")
      expect(new Stream(0, 1, 0.1).toString()).to.equal("[0,0.1...1]")
    })

    it("works in the negative direction", () => {
      expect(new Stream(3, 1, -1).toString()).to.equal("[3,2...1]")
    })

    it("works for describing infinite streams", () => {
      expect(new Stream(-Infinity, Infinity, 2).toString()).to.equal(
        "[-Infinity,-Infinity+2...Infinity]"
      )
      expect(new Stream(Infinity, -Infinity, -1).toString()).to.equal(
        "[Infinity,Infinity-1...-Infinity]"
      )
    })
  })

  describe("fromString()", () => {
    it("validates input", () => {
      // RegEx validation
      expect(() => Stream.fromString("")).to.throw()
      expect(() => Stream.fromString("1...10")).to.throw()
      expect(() => Stream.fromString("(1...10)")).to.throw()
      expect(() => Stream.fromString("{1...10}")).to.throw()
      expect(() => Stream.fromString("[1..10]")).to.throw()
      expect(() => Stream.fromString("[1,2,...3]")).to.throw()

      // Number validation
      expect(() => Stream.fromString("[f...10]")).to.throw()
      expect(() => Stream.fromString("[-infinity...infinity]")).to.throw()

      // Positive cases
      expect(() => Stream.fromString("[1...2]")).not.to.throw()
      expect(() => Stream.fromString("[1 ...2]")).not.to.throw()
      expect(() => Stream.fromString("[1... 2]")).not.to.throw()
      expect(() => Stream.fromString("[1 ... 2]")).not.to.throw()
      expect(() => Stream.fromString("[ 1 ... 2]")).not.to.throw()
      expect(() => Stream.fromString("[1 ... 2 ]")).not.to.throw()
      expect(() => Stream.fromString("[ 1 ... 2 ]")).not.to.throw()
      expect(() => Stream.fromString("[1,2...3]")).not.to.throw()
      expect(() => Stream.fromString("[1, 2...3]")).not.to.throw()
      expect(() => Stream.fromString("[3, 2...1]")).not.to.throw()
      expect(() => Stream.fromString("[1...Infinity]")).not.to.throw()
      expect(() => Stream.fromString("[-Infinity...Infinity]")).not.to.throw()
    })

    it("works with simple stepped streams", () => {
      const r1 = Stream.fromString("[1...10]")
      expect(r1.from).to.equal(1)
      expect(r1.to).to.equal(10)
      expect(r1.by).to.equal(1)
      expect(r1.isInfinite).to.be.false

      const r2 = Stream.fromString("[1,3...9]")
      expect(r2.from).to.equal(1)
      expect(r2.to).to.equal(9)
      expect(r2.by).to.equal(2)
      expect(r2.isInfinite).to.be.false

      const r3 = Stream.fromString("[1...Infinity]")
      expect(r3.from).to.equal(1)
      expect(r3.to).to.equal(Infinity)
      expect(r3.by).to.equal(1)
      expect(r3.isInfinite).to.be.true

      const r4 = Stream.fromString("[10, 9...1]")
      expect(r4.from).to.equal(10)
      expect(r4.to).to.equal(1)
      expect(r4.by).to.equal(-1)
      expect(r4.isInfinite).to.be.false
    })
  })

  describe("toArray()", () => {
    it("includes first and last", () => {
      const stream = new Stream(0, 1)

      expect(stream.toArray()).to.eql([0, 1])
    })

    it("respects 'by'", () => {
      const stream = new Stream(0, 10, 2)

      expect(stream.toArray()).to.eql([0, 2, 4, 6, 8, 10])
    })

    it("doesn't calculate an infinite stream", () => {
      const stream = new Stream()

      expect(stream.toArray()).to.eql([])
    })
  })

  describe("toTuple()", () => {
    it("properly gives back from, to, and by in that order", () => {
      expect(new Stream().toTuple()).to.eql([1, Infinity, 1])
      expect(new Stream(2, 10, 2).toTuple()).to.eql([2, 10, 2])
      expect(new Stream(3, 1, -1).toTuple()).to.eql([3, 1, -1])
    })

    it("can be used as input args for a copy Stream", () => {
      const original = new Stream()
      const args = original.toTuple()
      const copy = new Stream(...args)

      expect(original.from).to.equal(copy.from)
      expect(original.to).to.equal(copy.to)
      expect(original.by).to.equal(copy.by)
    })
  })

  describe("step()", () => {
    it("increments current by step", () => {
      const stream = new Stream()

      expect(stream.current).to.equal(1)
      stream.step()
      expect(stream.current).to.equal(2)
    })

    it("respects given 'by'", () => {
      const stream = new Stream(0, Infinity, 0.1)

      expect(stream.current).to.equal(0)
      stream.step()
      expect(stream.current).to.equal(0.1)
    })

    it("doesn't exceed end of stream", () => {
      const stream = new Stream(0, 1, 1)

      expect(stream.current).to.equal(0)
      stream.step()
      expect(stream.current).to.equal(1)
      stream.step()
      expect(stream.current).to.equal(1)
    })

    it("doesn't step over end", () => {
      const stream = new Stream(0, 1)

      expect(stream.current).to.equal(0)
      stream.step()
      expect(stream.current).to.equal(1)
      stream.step()
      expect(stream.current).to.equal(1)
    })

    it("can optionally take a number of steps", () => {
      const stream = new Stream(1, 10)

      expect(stream.current).to.equal(1)
      stream.step(10)
      expect(stream.current).to.equal(10)
    })

    it("works in the negative direction", () => {
      const stream = new Stream(0, -5, -1)

      expect(stream.current).to.equal(0)
      stream.step()
      expect(stream.current).to.equal(-1)
      stream.step(4)
      expect(stream.current).to.equal(-5)
    })
  })

  describe("isDone()", () => {
    it("recognizes when a stream has ended", () => {
      const r1 = new Stream(1, 1)
      expect(r1.isDone()).to.be.true

      const r2 = new Stream(0, 1)
      expect(r2.isDone()).to.be.false
      r2.step()
      expect(r2.isDone()).to.be.true
    })

    it("in both directions", () => {
      const stream = new Stream(0, -1, -1)

      expect(stream.isDone()).to.be.false
      stream.step()
      expect(stream.isDone()).to.be.true
    })

    it("recognizes that an infinite stream is never done", () => {
      const stream = new Stream()

      expect(stream.isDone()).to.be.false
      stream.step(Infinity)
      expect(stream.isDone()).to.be.false
    })
  })

  describe("take()", () => {
    it("returns an empty list when given an invalid count", () => {
      const stream = new Stream()

      expect(stream.take(0)).to.eql([])
      expect(stream.take(Infinity)).to.eql([])
    })

    it("properly continues from the current point", () => {
      const stream = new Stream()

      expect(stream.current).to.equal(1)
      stream.step()
      expect(stream.current).to.equal(2)
      expect(stream.take(2, true)).to.eql([2, 3])
      stream.step()
      expect(stream.current).to.equal(3)
      expect(stream.take(2, true)).to.eql([3, 4])
    })

    it("defaults to one item", () => {
      const stream = new Stream()

      expect(stream.take().length).to.equal(1)
    })

    it("starts from the beginning by default", () => {
      const stream = new Stream()

      stream.step(3)
      expect(stream.take(1)).to.eql([1])
      expect(stream.take(3)).to.eql([1, 2, 3])
    })

    it("only 'takes' what's available", () => {
      const stream = new Stream(1, 5)

      expect(stream.take(stream.items * 2)).to.eql([1, 2, 3, 4, 5])
    })

    it("works in the negative direction", () => {
      const stream = new Stream(3, 1, -1)

      expect(stream.take(3)).to.eql([3, 2, 1])
    })
  })

  describe("takeWhile()", () => {
    const isEven = (n: number): boolean => n % 2 === 0

    it("returns an empty list when given an invalid count", () => {
      const stream = new Stream()

      expect(stream.takeWhile(isEven, 0)).to.eql([])
      expect(stream.takeWhile(isEven, Infinity)).to.eql([])
    })

    it("properly continues from the current point", () => {
      const stream = new Stream()

      expect(stream.current).to.equal(1)
      stream.step()
      expect(stream.current).to.equal(2)
      expect(stream.takeWhile(isEven, 2, true)).to.eql([2, 4])
      stream.step()
      expect(stream.current).to.equal(3)
      expect(stream.takeWhile(isEven, 2, true)).to.eql([4, 6])
    })

    it("defaults to one item", () => {
      const stream = new Stream()

      expect(stream.takeWhile(isEven).length).to.equal(1)
    })

    it("starts from the beginning by default", () => {
      const stream = new Stream()

      stream.step(3)
      expect(stream.takeWhile(isEven, 1)).to.eql([2])
      expect(stream.takeWhile(isEven, 3)).to.eql([2, 4, 6])
    })

    it("only 'takes' what's available", () => {
      const stream = new Stream(2, 10)

      expect(stream.takeWhile(isEven, stream.items * 2)).to.eql([
        2,
        4,
        6,
        8,
        10
      ])
    })

    it("works in the negative direction", () => {
      const stream = new Stream(4, 0, -1)

      expect(stream.takeWhile(isEven, 3)).to.eql([4, 2, 0])
    })
  })

  describe("at()", () => {
    it("doesn't look for something out of range", () => {
      const stream = new Stream(1, 5)

      expect(stream.at(-1)).to.be.undefined
      expect(stream.at(stream.items + 1)).to.be.undefined
    })

    it("gets the value", () => {
      const stream = new Stream()

      stream.take(100)
      expect(stream.at(0)).to.equal(1)
      expect(stream.at(50)).to.equal(51)
      expect(stream.at(100)).to.equal(101)
    })
  })

  describe("contains()", () => {
    it("recognizes its predefined elements", () => {
      const start = 1
      const end = 10
      const stream = new Stream(start, end)

      expect(stream.contains(start)).to.be.true
      expect(stream.contains(end)).to.be.true
    })

    it("recognizes what's in and out of step", () => {
      const stream = new Stream(2, 6, 2)

      expect(stream.contains(2)).to.be.true
      expect(stream.contains(3)).to.be.false
      expect(stream.contains(4)).to.be.true
      expect(stream.contains(5)).to.be.false
      expect(stream.contains(6)).to.be.true
    })
  })

  describe("isSupersetOf()", () => {
    it("works for streams with the same step and direction", () => {
      expect(new Stream().isSupersetOf(new Stream(1, 10))).to.be.true
      expect(new Stream(0, Infinity, 2).isSupersetOf(new Stream(4, 10, 2))).to
        .be.true

      expect(new Stream(1, 5).isSupersetOf(new Stream(0, 4))).to.be.false
      expect(new Stream(1, 3, 2).isSupersetOf(new Stream(1, 3))).to.be.false
      expect(new Stream().isSupersetOf(new Stream(Infinity, 0, -1))).to.be.false
    })

    it("works for streams that contain the same elements (though not technically the same step)", () => {
      expect(
        new Stream(2, Infinity, 2).isSupersetOf(new Stream(4, Infinity, 4))
      ).to.be.true
      expect(new Stream().isSupersetOf(new Stream(1, Infinity, 3))).to.be.true
    })

    it("works for a practical example", () => {
      const integers = new Stream()
      const oddNumbers = new Stream(1, Infinity, 2)
      const evenNumbers = new Stream(2, Infinity, 2)

      expect(integers.isSupersetOf(oddNumbers)).to.be.true
      expect(integers.isSupersetOf(evenNumbers)).to.be.true
      expect(oddNumbers.isSupersetOf(evenNumbers)).to.be.false
      expect(evenNumbers.isSupersetOf(oddNumbers)).to.be.false
    })
  })

  describe("zip()", () => {
    it("zips up length of given array", () => {
      const array = [1, 2, 3]
      const stream = new Stream()

      expect(stream.zip(array).length).to.equal(array.length)
    })

    it("matches types properly", () => {
      const array = ["a", "b", "c"]
      const stream = new Stream()

      expect(stream.zip(array)).to.eql([
        ["a", 1],
        ["b", 2],
        ["c", 3]
      ])
    })

    it("stops when either side of the zip is out of entries", () => {
      const array = ["a", "b", "c"]
      const stream = new Stream(1, 2)

      expect(stream.zip(array)).to.eql([
        ["a", 1],
        ["b", 2]
      ])
    })
  })

  describe("use cases", () => {
    it("works for finding primes with the Sieve of Eratosthenes", () => {
      const naturalNumbers = new Stream(3)
      const primes = [2]
      const isFactor = (a: number, b: number): boolean => a % b === 0
      const sieve = (n: number): boolean => {
        const isPrime = primes.reduce((passed, prime) => {
          if (!passed) return passed
          if (isFactor(n, prime)) return false
          return passed
        }, true)
        if (isPrime) primes.push(n)

        return isPrime
      }
      naturalNumbers.takeWhile(sieve, 100)

      expect(primes[primes.length - 1]).to.equal(547)
      // Last benchmark: 5ms
    })
  })
})
