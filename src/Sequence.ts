import * as Utils from "./utils"
import Stream from "./Stream"
import {IDS, Maybe, Memo, Range, Tuple} from "./types"

type GeneratorFn<T> = (
  index: number,
  memoAccessor: (index: number) => Maybe<T>
) => T

interface SequenceOptions {
  cacheToIndex?: number
  isFinite?: boolean
  finiteBoundary?: Range
}

export default class Sequence<T> implements IDS<T> {
  private _index: number
  private _memo: Memo<T>
  private _finiteBoundary?: Range

  readonly initial: T[]
  readonly generator: GeneratorFn<T>
  readonly isInfinite: boolean

  current: T

  constructor(
    initial: T[],
    generator: GeneratorFn<T>,
    options?: SequenceOptions
  ) {
    const {
      cacheToIndex = initial.length - 1,
      isFinite = false,
      finiteBoundary
    } = options || {}
    this.initial = initial
    this._index = 0
    this.current = this.initial[this._index]
    this.generator = generator
    this.isInfinite = !isFinite
    this._memo = new Map() as Memo<T>
    this._initializeMemo()
    this._precacheTo(cacheToIndex)
    if (finiteBoundary) this._finiteBoundary = finiteBoundary
  }

  private _initializeMemo = (): Memo<T> => {
    this.initial.forEach((value, index) =>
      Utils.memoize(this._memo, index, value)
    )
    return this._memo
  }

  private _precacheTo = (index: number): Memo<T> => {
    if (index <= this._memo.size) return this._memo
    for (let i = this._memo.size; i <= index; i++) this.at(i)
    return this._memo
  }

  step = (times = 1): T => {
    this._index += times
    const nextValue = this.at(this._index)
    if (nextValue) this.current = nextValue
    return this.current
  }

  at = (index: number): Maybe<T> => {
    if (index < 0) return undefined
    if (this._memo.has(index)) return this._memo.get(index)
    const value = this.generator(
      index,
      (key: number): Maybe<T> => this._memo.get(key)
    )
    Utils.memoize(this._memo, index, value)
    return value
  }

  isDone = (): boolean =>
    this.isInfinite ||
    (!!this._finiteBoundary &&
      !!this._finiteBoundary.max &&
      this._index >= this._finiteBoundary.max - 1)

  generate = (from: number, to: number): Maybe<T>[] =>
    new Stream(from, to).toArray().map(index => this.at(index))

  take = (count = 1, fromCurrent = false): T[] => {
    if (Utils.outOfBounds(count)) return []
    const offset = fromCurrent ? this._index : 0
    const maxedCount =
      this._finiteBoundary && this._finiteBoundary.max
        ? Math.max(this._finiteBoundary.max, count)
        : count
    return Utils.arrayGenerator<T>(maxedCount, (_, index) =>
      this.at(index + offset)
    ).filter(maybe => maybe !== undefined) as T[]
  }

  takeWhile = (
    condition: (item: T) => boolean,
    count = 1,
    fromCurrent = false
  ): T[] => {
    if (Utils.outOfBounds(count)) return []
    const copy = new Sequence(this.initial, this.generator)
    if (fromCurrent && this._index > 0) copy.step(this._index)
    return Utils.stepWhile(copy, condition, count)
  }

  zip = <U>(array: U[]): Tuple<U, T>[] =>
    Utils.zipMap(array, this, array.length)
}
